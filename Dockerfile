FROM mongo
COPY ./init.js /docker-entrypoint-initdb.d/
CMD ["--logpath", "/var/log/mongodb/mongodb.log"]
EXPOSE 27017

